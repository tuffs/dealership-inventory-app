class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string :make
      t.string :model
      t.integer :year
      t.integer :mileage
      t.string :interior_color
      t.string :exterior_color
      t.string :condition
      t.text :description
      t.integer :price

      t.timestamps
    end
  end
end
