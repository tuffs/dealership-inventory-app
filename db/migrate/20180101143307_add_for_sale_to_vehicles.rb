class AddForSaleToVehicles < ActiveRecord::Migration[5.1]
  def change
  	add_column :vehicles, :for_sale, :boolean, :default => true
  end
end
