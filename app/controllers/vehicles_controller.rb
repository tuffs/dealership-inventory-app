class VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @vehicles = Vehicle.all.where(for_sale: true)
  # add in later w/ will_paginate: .paginate(:page => params[:page], :per_page => 10)
  end

  def all
    @vehicles = Vehicle.all
  end

  def new
    @vehicle = Vehicle.new
  end

  def create
    @vehicle = Vehicle.new(vehicle_params)

    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to @vehicle, notice: "The vehicle was created succesfully." }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @vehicle.update(vehicle_params)
        format.html { redirect_to @vehicle, notice: "The vehicle was updated succesfully." }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def destroy
    @vehicle.destroy
    redirect_to root_path, notice: "The vehicle  was succesfully marked as sold."
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow trusted parameters
    def vehicle_params
      params.require(:vehicle).permit(:make, :model, :year, :mileage, :interior_color, :exterior_color, :condition, :description, :price, :for_sale)
    end
end
