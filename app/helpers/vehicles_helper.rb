module VehiclesHelper
	def button_text_vehicles
		if controller.action_name == "new"
			return "Add this Vehicle to Inventory"
		elsif controller.action_name == "edit"
			return "Edit Vehicle Information"
		else
			return "Add A Vehicle"
		end
	end

	def inventory_title
		if controller.action_name == "index"
			return "Vehicles For Sale"
		elsif controller.action_name = "all"
			return "All Vehicles, Sold and Unsold"
		end
	end
end
