Rails.application.routes.draw do
  get 'vehicles/all'
  resources :vehicles
  devise_for :users
  get 'pages/index'
  root to: 'pages#index'
end
