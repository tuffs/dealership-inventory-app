# ECMC INVENTORY APP
============================================================

### MVC NAMING CONVENTION AND MODEL SETUP -> 

+ VEHICLE MODEL: Vehicle
resource generator
- rails g resource Vehicle make model year:integer mileage:integer interior_color exterior_color condition description:text price:integer

	vehicle:
	----------------------
	-	make
	- model
	- year:integer
	- mileage:integer
	- interior_color
	- exterior_color
	- condition
	- description:text
	- price:integer

============================================================

Vehicle has_many :photos, :dependent => :destroy
Vehicle accepts_nested_attributes_for :photos

============================================================

## Photo Model < Vehicle Model
Photo belongs_to Vehicle
vehicle_id:integer
image
description:text



